package model;




public class Company implements Taxable{
	private String name;
	private double receive;
	private double expenses;
	
	public Company(String aName, double anReceive, double anExpenses){
		this.name = aName;
		this.receive = anReceive;
		this.expenses = anExpenses;
	}
	
	public String getName(){
		return this.name;
	}
	
	public double getReceive(){
		return this.receive;
	}
	
	public double getExpenses(){
		return this.expenses;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double sum = 0;
		sum = (this.getReceive() - this.getExpenses())*0.30;
		return sum;
	}
	
	public double getProfit(){
		double sum = 0;
		sum = receive - expenses;
		return sum;
		
	}
	
	public String toString(){
		return this.name + " " + this.receive + " " + this.expenses ;
		
	}
}
