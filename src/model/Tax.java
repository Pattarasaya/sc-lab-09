package model;

import java.util.Comparator;

public class Tax implements Taxable,Comparator<Tax>{

	@Override
	public int compare(Tax t1, Tax t2) {
		// TODO Auto-generated method stub
		if(t1.getTax() < t2.getTax())
			return -1;
		else if(t1.getTax() > t2.getTax())
			return 1;
		else
			return 0;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return 0;
	}
}
