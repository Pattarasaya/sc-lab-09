package model;


public class Person implements Taxable,Comparable<Person>{
	private String name;
	private double yearlyIncome;

	public Person(String name, double yearlyIncome) {
		this.name = name;
		this.yearlyIncome = yearlyIncome;
	}

	public String getName() {
		return this.name;
	}


	public double getYearlyIncome() {
		return this.yearlyIncome;
	}

	@Override
	public double getTax() {
		double income = this.getYearlyIncome();
		double result = 0;

		if (income > 300000) {
			result += 0.05 * 300000;
			income -= 300000;
			result += 0.10 * income;
		}else{
			result += 0.05 * income;
		}
		return result;
	}

	@Override
	public int compareTo(Person obj) {
		// TODO Auto-generated method stub
		Person other = (Person) obj;
		if(this.getYearlyIncome() < other.getYearlyIncome())
			return -1;
		else if(this.getYearlyIncome() > other.getYearlyIncome())
			return 1;
		else
			return 0;
	}
	
	public String toString(){
		return this.name + " " + this.yearlyIncome ;	
	}
}
