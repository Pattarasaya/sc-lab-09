package model;

import java.util.ArrayList;
import java.util.Collections;



public class TesterMain {
	
	
	public static void main(String[] args) {
	
		TesterMain tester = new TesterMain();
			tester.testPerson();
			tester.testProduct();
			tester.testCompanies();
//			tester.testTax();
	}
	
	private void testPerson(){
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Wirote", 500000));
		persons.add(new Person("Saran", 300000));
		persons.add(new Person("Ratana", 75000));
		Collections.sort(persons);
		for(Person p:persons){
			System.out.println(p);
		}
		System.out.println("----------------------------------------------");
	}
	
	private void testProduct(){
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("Rotring", 199));
		products.add(new Product("Table", 2075));
		products.add(new Product("Paper A4", 560));
		Collections.sort(products);
		for(Product p:products){
			System.out.println(p);
		}
		System.out.println("----------------------------------------------");
	}
	
	private void testCompanies(){
		ArrayList<Company> companies = new ArrayList<Company>();
		companies.add(new Company("Sea Fly Services Company Limited", 8000000, 2000000));
		companies.add(new Company("Earth Telecom co.,Ltd", 2000000, 500000));
		companies.add(new Company("Wong-sa-whang corporation public company limited", 4000000, 800000));

		Collections.sort(companies,new EarningComparator());
		for(Company c : companies){
			System.out.println(c);
		}
		System.out.println("----------------------------------------------");
	
		Collections.sort(companies,new ExpenseComparator());
		for(Company c : companies){
			System.out.println(c);
		}
		System.out.println("----------------------------------------------");
		
		Collections.sort(companies,new ProfitComparator());
		for(Company c : companies){
			System.out.println(c.getProfit());
		}
		
		System.out.println("----------------------------------------------");

	}
		
//	private void testTax(){
//		ArrayList<Taxable> tax = new ArrayList<Taxable>();
//		tax.add(new Person("Sucha", 500000));
//		tax.add(new Company("Sara Crop International co.,Ltd", 1000000, 800000));
//		tax.add(new Product("NoteBook", 25000));
//		
//		Collections.sort(tax);
//		for(Taxable t : tax){
//			System.out.println(t.getTax());
//		}
//	}
}
