package treetraversal;

import java.util.List;


public class ReportConsole {

	public void display(Node tree, Traversal traversal) {
		
		List<Node> walk = traversal.traverse(tree);
		String name = traversal.getClass().getSimpleName();
		System.out.print("Traverse with "+name+": ");
		for (Node x : walk) {
			System.out.print(x.getValue() + " ");
		}
		System.out.println();
	}

}
