package treetraversal;

import java.util.ArrayList;
import java.util.List;


public class PreOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node node) {
		List<Node> result = new ArrayList<Node>();
		
		if (node == null) {
			return new ArrayList<Node>(); 
		}
		
		result.add(node);
		result.addAll(traverse(node.getLeft()));
		result.addAll(traverse(node.getRight()));
		
		return result;
	}

}
