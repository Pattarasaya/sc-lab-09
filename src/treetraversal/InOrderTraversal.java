package treetraversal;

import java.util.ArrayList;
import java.util.List;


public class InOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node node) {
		List<Node> result;
		
		if (node == null) {
			return new ArrayList<Node>(); 
		}
		
		result = traverse(node.getLeft());
		result.add(node);
		result.addAll(traverse(node.getRight()));
		
		return result;
	}

}
